import React, { Component } from 'react'
import { Link } from "react-router-dom";


export default class AcknowledgementPage extends Component {
    constructor() {
        super();
        this.state = {
        }

    }


    render() {
        return (
            <div>
                <h1 className={"thanks-message"}>Thank you for asking!</h1>
                <Link to="/askme" className={"go-home-link"}>Ask More</Link>
            </div>
        )
    }
}
