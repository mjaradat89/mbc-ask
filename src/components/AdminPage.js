import React, { Component } from 'react'
import { addQuestion, showQuestion } from 'actions';
import { connect } from 'react-redux';
import { Question, LoginForm } from 'components'
import { Config } from 'public'

const ws = new WebSocket(Config.URL)


class AdminPage extends Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: false,
            name: 'Admin',
            password: "mbc@1234",
            questions: [],
        }
        this.apporveQuestion = this.apporveQuestion.bind(this)
        this.onSubmitLoginForm = this.onSubmitLoginForm.bind(this)
    }

    componentDidMount() {
        ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            console.log('connected')
        }
        console.log(ws)

        ws.onmessage = (evt) => {
            // on receiving a question, add it to the list ofquestions
            const { addQuestion } = this.props
            const question = JSON.parse(evt.data)
            addQuestion(question)
        }

        ws.onclose = () => {
            console.log('disconnected')
            // automatically try to reconnect on connection loss
            this.setState({
                ws: new WebSocket(URL),
            })
        }
    }

    apporveQuestion(event, question, index) {
        const { showQuestion } = this.props;
        event.preventDefault();
        document.querySelectorAll(`.question-${index} .admin-controls`).forEach(function (button, x) {
            button.classList.add("hide");
        })

        document.querySelector(`.question-${index} .approved`).classList.remove("hide")
        question.canShow = true;
        ws.send(JSON.stringify(question))
        showQuestion(question)
    }

    skipQuestion(event) {
        event.target.parentElement.remove()
    }

    onSubmitLoginForm(userName, password) {
        if (!userName || !password) {
            alert("Please enter your user and password ")
            return;
        }

        if (userName === this.state.name && password === this.state.password) {
            this.setState({ isLoggedIn: true });
        }

    }

    render() {
        const { questions } = this.props;
        return (
            <div>
                {!this.state.isLoggedIn &&
                    <div className="login-container">
                        <LoginForm onSubmitLoginForm={(userName, password) => { this.onSubmitLoginForm(userName, password) }} />
                    </div>
                }

                {!(questions.length) && this.state.isLoggedIn ?

                    <h1 className={"no-questions"}>No Questions To Show</h1>
                    :
                    <div></div>
                }
                {this.state.isLoggedIn &&
                    questions.map((question, index) =>
                        <div key={index} className={`pending-question-container question-${index}`}>

                            <Question
                                question={question.question}
                                name={question.name}
                            />
                            <button
                                className={'admin-controls show-question-button'}
                                onClick={(event) => { this.apporveQuestion(event, question, index) }}>
                                Show
                        </button>
                            <button
                                className={'admin-controls skip-question-button'}
                                onClick={(event) => { this.skipQuestion(event, question) }}>
                                Skip
                        </button>
                            <div className={"approved hide"}></div>
                        </div>
                    )}

            </div>
        )
    }
}

const mapStateToProps = ({ UserQuestions: { questions } }) => ({
    questions
})

export default connect(mapStateToProps, { addQuestion, showQuestion })(AdminPage);

