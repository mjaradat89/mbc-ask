import React, { Component } from 'react'
import { addQuestion } from 'actions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import { QuestionField } from 'components'
import { Config } from 'public'


const ws = new WebSocket(Config.URL)

class AskPage extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            questions: [],
            redirect: false
        }

        this.submitQuestion = this.submitQuestion.bind(this)
        this.setRedirect = this.setRedirect.bind(this)
    }

    componentDidMount() {
        ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            console.log('connected')
        }

        ws.onmessage = (evt) => {
            // on receiving a question, add it to the list ofquestions
            const { addQuestion } = this.props
            const question = JSON.parse(evt.data)
            addQuestion(question)
        }

        ws.onclose = () => {
            // automatically try to reconnect on connection loss
            this.setState({
                ws: new WebSocket(URL),
            })
        }
    }

    setRedirect() {
        this.setState({
            redirect: true
        })
    }


    submitQuestion(questionString) {
        // on submitting the QuestionField form, send the question, add it to the list and reset the input
        if (!questionString) {
            alert("Please ask a question!")
            return;
        }
        const { questions, addQuestion } = this.props
        const question = { id: questions.length + 1, name: this.state.name, question: questionString, canShow: false }
        ws.send(JSON.stringify(question))
        addQuestion(question)
        this.setRedirect();
    }

    render() {
        return (
            <div>
                {this.state.redirect ?
                    <Redirect to='/acknowledgement' />
                    :
                    <div>
                        <QuestionField
                            ws={ws}
                            onSubmitQuestion={questionString => this.submitQuestion(questionString)}
                        />
                    </div>
                }

            </div>
        )
    }
}

const mapStateToProps = ({ UserQuestions: { questions } }) => ({
    questions
})

export default connect(mapStateToProps, { addQuestion })(AskPage);

